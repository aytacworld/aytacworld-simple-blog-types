import { Comment } from './comment';

export interface Post {
  comments: Array<string | Comment>;
  createdAt: Date;
  id: string;
  message: string;
  updatedAt: Date;
  publishDate: Date;
  slug: string;
  tags: Array<string>;
  title: string;
}
