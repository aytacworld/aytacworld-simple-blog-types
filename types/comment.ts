import { Post } from './post';

export interface Comment {
  _parent: string | Comment;
  _post: string | Post;
  createdAt: Date;
  email: string;
  id: string;
  message: string;
  name: string;
  replies: Array<Comment>;
}
